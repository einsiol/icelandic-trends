const { exec } = require('child_process')
const fetch = require('node-fetch')
const fs = require('fs')

const argv = JSON.parse(process.env.npm_config_argv)
const configString = argv.original.indexOf('-text')
const stringToFind = configString >= 0 ? argv.original[configString + 1] : 'þeir'

const { CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN_KEY, ACCESS_TOKEN_SECRET } = process.env

const authentication = () => new Promise((resolve, reject) => {
  /* eslint-disable-next-line quotes */
  const curl = exec(`curl -u '${CONSUMER_KEY}:${CONSUMER_SECRET}' \
  --data 'grant_type=client_credentials' \
  'https://api.twitter.com/oauth2/token'`)

  curl.stdout.on('data', (data) => {
    resolve(JSON.parse(data))
  })
})


const searchForTweets = async (token) => {
  const body = {
    query: 'þetta',
    maxResults: '100',
    fromDate: '201910010000',
    toDate: '201910122359'
  }

  const response = await fetch('https://api.twitter.com/1.1/tweets/search/30day/search.json', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${token}`,
      'tweet_mode': 'extended'
    }
  })

  const data = await response.json()

  if (response.ok) {
    return data
  }

  throw Error(data.message)
}

const getContent = async () => {
  try {
    const bearer = await authentication()

    const { results } = await searchForTweets(bearer.access_token)

    const stripped = results.map(({
      user: { id, name, screen_name: screenName }
    }) => ({
      id: { name, screenName }
    }))

    fs.writeFile('users.json', JSON.stringify(results, null, 2), 'utf8', (err) => {
      if (err) {
        return console.log(err)
      }

      console.log('file saved')
    })
  }
  catch (error) {
    console.error(error)
  }
}

getContent()


